FROM ubuntu:18.04 AS build

# Download tini
# ADD https://github.com/krallin/tini/releases/download/v0.17.0/tini-static /sbin/tini
# RUN chmod +x /sbin/tini

# Download Terraria server
RUN apt-get update && apt-get install -yq unzip

ADD https://terraria.org/server/terraria-server-1353.zip /tmp/server.zip
RUN unzip /tmp/server.zip -d /server
RUN chmod +x /server/1353/Linux/TerrariaServer.bin.x86_64

ADD https://github.com/blushiemagic/tModLoader/releases/download/v0.11.7.6/tModLoader.Linux.v0.11.7.6.zip /tmp/mods.zip
RUN unzip /tmp/mods.zip -d /modloader
RUN chmod +x /modloader/tModLoaderServer*


# Multi-stage build to keep size small
FROM ubuntu:18.04

RUN apt-get update && apt-get install -yq gnupg ca-certificates
RUN apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 3FA7E0328081BFF6A14DA29AA6A19B38D3D831EF
RUN echo "deb https://download.mono-project.com/repo/ubuntu stable-bionic main" | tee /etc/apt/sources.list.d/mono-official-stable.list
RUN apt-get update && apt-get install -yq mono-complete

#COPY --from=build /sbin/tini /sbin/tini
COPY --from=build /server/1353/Linux/ /server
COPY --from=build /modloader/* /server/

# Hack to force this to use the system mono
RUN rm /server/System*.dll

VOLUME /persistence
EXPOSE 7777

ENV MONO_IOMAP=all

ENTRYPOINT ["mono", "/server/tModLoaderServer.exe"]
CMD [ "-savedirectory", "/persistence", "-players", "8", "-noupnp" ]
